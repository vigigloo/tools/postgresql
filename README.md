# PostgreSQL deployment using Bitnami PostgreSQL HA

For production use, prefer using the [PostgreSQL deployments](https://gitlab.com/vigigloo/tools/crunchydata-pgo-cluster) that rely on the [PostgreSQL Operator](https://gitlab.com/vigigloo/tools/crunchydata-pgo).

This module is still suitable for tests and development environments that favor a simpler setup.
