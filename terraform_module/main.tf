resource "random_password" "superuser-password" {
  length           = var.random_complexity
  override_special = "@:.~"
}

resource "helm_release" "postgresql" {
  chart           = "postgresql-ha"
  repository      = "https://charts.bitnami.com/bitnami"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = concat([
    file("${path.module}/postgresql.yaml"),
  ], var.values)

  set {
    type  = "string"
    name  = "postgresql.username"
    value = var.superuser-username
  }

  set {
    type  = "string"
    name  = "postgresql.password"
    value = random_password.superuser-password.result
  }

  set {
    type  = "string"
    name  = "postgresql.database"
    value = var.database-name
  }

  set {
    type  = "string"
    name  = "postgresqlImage.repository"
    value = var.image_repository
  }

  set {
    type  = "string"
    name  = "postgresqlImage.tag"
    value = var.image_tag
  }

  set {
    name  = "postgresql.resources.limits.cpu"
    value = var.postgresql_limits_cpu
  }

  set {
    name  = "postgresql.resources.limits.memory"
    value = var.postgresql_limits_memory
  }

  set {
    name  = "postgresql.resources.requests.cpu"
    value = var.postgresql_requests_cpu
  }

  set {
    name  = "postgresql.resources.requests.memory"
    value = var.postgresql_requests_memory
  }

  set {
    name  = "pgpool.resources.limits.cpu"
    value = var.pgpool_limits_cpu
  }

  set {
    name  = "pgpool.resources.limits.memory"
    value = var.pgpool_limits_memory
  }

  set {
    name  = "pgpool.resources.requests.cpu"
    value = var.pgpool_requests_cpu
  }

  set {
    name  = "pgpool.resources.requests.memory"
    value = var.pgpool_requests_memory
  }
}
