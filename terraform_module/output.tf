output "superuser-password" {
  value      = random_password.superuser-password.result
  depends_on = [helm_release.postgresql]
}

output "superuser-username" {
  value      = var.superuser-username
  depends_on = [helm_release.postgresql]
}

output "database" {
  value      = var.database-name
  depends_on = [helm_release.postgresql]
}

output "domain" {
  value      = "${var.chart_name}-postgresql-ha-pgpool.${var.namespace}.svc.cluster.local"
  depends_on = [helm_release.postgresql]
}

output "port" {
  value      = "5432"
  depends_on = [helm_release.postgresql]
}

output "npgsql-connexion-string" {
  value      = "Server=${var.chart_name}-postgresql-ha-pgpool.${var.namespace}.svc.cluster.local;Port=5432;User Id=${var.superuser-username};Password=${random_password.superuser-password.result};Database=${var.database-name};"
  depends_on = [helm_release.postgresql]
}

output "uri-connexion-string" {
  value      = "postgresql://${var.superuser-username}:${random_password.superuser-password.result}@${var.chart_name}-postgresql-ha-pgpool.${var.namespace}.svc.cluster.local:5432/${var.database-name}"
  depends_on = [helm_release.postgresql]
}
