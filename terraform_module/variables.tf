variable "namespace" {
  type = string
}

variable "chart_name" {
  type = string
}

variable "chart_version" {
  type    = string
  default = "7.9.6"
}

variable "values" {
  type    = list(string)
  default = []
}

variable "image_repository" {
  type    = string
  default = "bitnami/postgresql-repmgr"
}

variable "image_tag" {
  type    = string
  default = "13.4.0"
}

variable "helm_force_update" {
  type    = bool
  default = false
}

variable "helm_recreate_pods" {
  type    = bool
  default = false
}

variable "helm_cleanup_on_fail" {
  type    = bool
  default = false
}

variable "helm_max_history" {
  type    = number
  default = 0
}

variable "postgresql_limits_cpu" {
  type    = string
  default = "1000m"
}

variable "postgresql_limits_memory" {
  type    = string
  default = "1024Mi"
}

variable "postgresql_requests_cpu" {
  type    = string
  default = "500m"
}

variable "postgresql_requests_memory" {
  type    = string
  default = "512Mi"
}


variable "pgpool_limits_cpu" {
  type    = string
  default = "300m"
}

variable "pgpool_limits_memory" {
  type    = string
  default = "512Mi"
}

variable "pgpool_requests_cpu" {
  type    = string
  default = "100m"
}

variable "pgpool_requests_memory" {
  type    = string
  default = "256Mi"
}

variable "superuser-username" {
  type    = string
  default = "postgres"
}

variable "database-name" {
  type    = string
  default = "postgres"
}

variable "random_complexity" {
  type    = number
  default = 32
}
